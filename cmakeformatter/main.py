#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 19:37:36 2021

@author: aramgrigoryan
"""

import os
import subprocess
from tqdm import tqdm

CMAKELISTS_FILE = "CMakeLists.txt"

def get_files(current_dir="."):
    """
    Returns list of CMakeLists.txt files

    Parameters
    ----------
    current_dir: str

    Returns
    -------
    list.

    """
    list_of_files = []
    
    for root, dirs, files in os.walk(current_dir):
        for file in files:
            if file==CMAKELISTS_FILE:
                list_of_files.append(os.path.join(root, file))
                
    return list_of_files

def format_files(files):
    """
    Calls cmake-format on each file in files

    Parameters
    ----------
    files : list
        DESCRIPTION.

    Returns
    -------
    None.

    """
    for file in tqdm(files):
        subprocess.run(["cmake-format", "-i", file])

def main():
    """
    Runs Script on all files

    Returns
    -------
    None.

    """
    format_files(get_files())

if __name__ == "__main__":
    main()
    