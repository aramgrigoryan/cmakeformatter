"""
__version__.py
~~~~~~~~~~~~~~

Information about the current version of the cmakeformatter package.
"""

__title__ = 'cmakeformatter'
__description__ = 'Helper package for cmake-format'
__version__ = '0.0.1'
__author__ = 'Aram Grigoryan'
__author_email__ = 'aramgrigoryan@protonmail.ch'
__license__ = 'GPL 3'
__url__ = 'https://gitlab.com/aramgrigoryan/cmakeformatter'
