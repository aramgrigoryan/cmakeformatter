# CMake file formatter

A simple helper script for [cmake-format](https://github.com/cheshirekow/cmake_format) .

Runs cmake-format in a given project directory recursively and in-place!

Install with pip:

>> pip3 install --user git+https://gitlab.com/aramgrigoryan/cmakeformatter.git

And run inside your project directory

>> cmakeformatter


